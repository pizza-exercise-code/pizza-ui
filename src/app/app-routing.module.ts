import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { ManagerPageComponent } from './pages/manager-page/manager-page.component';
import { NotFoundPageComponent } from './pages/not-found-page/not-found-page.component';
import { PizzaInformationPageComponent } from './pages/pizza-information-page/pizza-information-page.component';
import { StorePageComponent } from './pages/store-page/store-page.component';

const routes: Routes = [
  { path: "", component: HomePageComponent },
  { path: "menu", component: StorePageComponent },
  { path: "menu/:id", component: PizzaInformationPageComponent },
  { path: "manage-pizzas", component: ManagerPageComponent },
  { path: '**', pathMatch: 'full', component: NotFoundPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
