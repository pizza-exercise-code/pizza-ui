import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ResponseModel } from '../models/responseModel';
import { PizzaModel } from '../models/pizzaModel';
import { environment } from 'src/environments/environment';
import { PizzaFlavorModel } from '../models/pizzaFlavorModel';
import { ToppingModel } from '../models/toppingModel';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class PizzaService {
  private basePath = environment.PIZZA_API;
  constructor(private http: HttpClient) { }

  getPizzas(): Observable<ResponseModel<PizzaModel[]>> {
    const url = `${this.basePath}pizzas`;
    return this.http.get<ResponseModel<PizzaModel[]>>(url, httpOptions);
  }

  getPizzasById(pizzaId: string): Observable<ResponseModel<PizzaModel>> {
    const url = `${this.basePath}pizzas/${pizzaId}`;
    return this.http.get<ResponseModel<PizzaModel>>(url, httpOptions);
  }

  getPizzaAndToppings(pizzaId: string): Observable<ResponseModel<PizzaFlavorModel>> {
    const url = `${this.basePath}pizzas/${pizzaId}/toppings`;
    return this.http.get<ResponseModel<PizzaFlavorModel>>(url, httpOptions);
  }

  postPizza(pizzaModel: PizzaModel): Observable<ResponseModel<PizzaModel>> {
    const url = `${this.basePath}pizzas`;
    return this.http.post<ResponseModel<PizzaModel>>(url, pizzaModel, httpOptions);
  }

  deletePizza(pizzaId: string): Observable<ResponseModel<boolean>> {
    const url = `${this.basePath}pizzas/${pizzaId}`;
    return this.http.delete<ResponseModel<boolean>>(url, httpOptions);
  }

  updatePizza(pizza: PizzaModel): Observable<ResponseModel<PizzaFlavorModel>> {
    const url = `${this.basePath}pizzas/${pizza.id}`;
    return this.http.put<ResponseModel<PizzaFlavorModel>>(url, pizza, httpOptions);
  }

  addToppingToPizza(pizzaId: string, toppings: ToppingModel[]): Observable<ResponseModel<PizzaFlavorModel>> {
    const url = `${this.basePath}pizzas/${pizzaId}/add-toppings-to-pizza`;
    return this.http.post<ResponseModel<PizzaFlavorModel>>(url, toppings, httpOptions);
  }
}
