import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ResponseModel } from '../models/responseModel';
import { ToppingModel } from '../models/toppingModel';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ToppingService {
  basePath = environment.PIZZA_API;
  constructor(private http: HttpClient) { }

  getToppings(): Observable<ResponseModel<ToppingModel[]>> {
    const url = `${this.basePath}toppings`;
    return this.http.get<ResponseModel<ToppingModel[]>>(url, httpOptions);
  }

  getToppingById(toppingId: string): Observable<ResponseModel<ToppingModel>> {
    const url = `${this.basePath}toppings/${toppingId}`;
    return this.http.get<ResponseModel<ToppingModel>>(url, httpOptions);
  }

  postTopping(topping: ToppingModel): Observable<ResponseModel<ToppingModel>> {
    const url = `${this.basePath}toppings`;
    return this.http.post<ResponseModel<ToppingModel>>(url, topping, httpOptions);
  }

  deleteTopping(toppingId: string): Observable<ResponseModel<boolean>> {
    const url = `${this.basePath}toppings/${toppingId}`;
    return this.http.delete<ResponseModel<boolean>>(url, httpOptions);
  }

  updateTopping(topping: ToppingModel): Observable<ResponseModel<ToppingModel>> {
    const url = `${this.basePath}toppings/${topping.id}`;
    return this.http.put<ResponseModel<ToppingModel>>(url, topping, httpOptions);
  }
}
