export const navbarLogo = {
  pizzaLabel: "Pizza",
  storeLabel: "Store",
  logoUrl: "https://cdn-icons-png.flaticon.com/512/3132/3132693.png"
}

export const navbarItems = [
  {
    name: "Home",
    url: "/"
  },
  {
    name: "Menu",
    url: "/menu"
  },
  {
    name: "Kitchen",
    url: "/manage-pizzas"
  }
]