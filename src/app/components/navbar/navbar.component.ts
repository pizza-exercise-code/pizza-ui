import { Component, OnInit } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { navbarItems, navbarLogo } from './navbar-mock';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  navigation = navbarLogo;
  items = navbarItems;
  itemSelected = navbarItems[0];
  constructor(private router: Router) { }

  ngOnInit(): void {
    this.changeItemSelectedOnChangeUrl();
  }

  changeItemSelectedOnChangeUrl () {
    this.router.events.subscribe((route) => {
      if (route instanceof NavigationStart) {
        const item = this.items.find(i => i.url == (this.pizzaSubDomain(route)));
        this.itemSelected = item;
      }
    });
  }

  pizzaSubDomain(route) {
    let result = route.url;
    if (route.url.toString().includes('menu/')) {
      result = "/menu";
    }
    return result;
  }

  onSelectItem(item) {
    this.itemSelected = item;
    this.router.navigate([item.url]);
  }
}
