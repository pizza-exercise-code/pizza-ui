import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ToppingUpdateComponent } from './topping-update.component';

describe('ToppingUpdateComponent', () => {
  let component: ToppingUpdateComponent;
  let fixture: ComponentFixture<ToppingUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ToppingUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ToppingUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
