import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToppingModel } from 'src/app/models/toppingModel';
import { ToppingService } from 'src/app/services/topping.service';

@Component({
  selector: 'app-topping-update',
  templateUrl: './topping-update.component.html',
  styleUrls: ['./topping-update.component.css']
})
export class ToppingUpdateComponent implements OnInit {
  @Output() responseToppingPut = new EventEmitter<ToppingModel>();
  @Input() topping: ToppingModel = new ToppingModel;
  @ViewChild('closebutton') closebutton;
  form: FormGroup;
  constructor(private formBuilder: FormBuilder,
    private ToppingService: ToppingService) { }

  ngOnInit(): void {
    this.formValidator();
  }

  onSubmit() {
    this.validateAllFormFields(this.form);
    if (this.form.valid) {
      this.ToppingService.updateTopping(this.topping).subscribe(response => {
        this.responseToppingPut.emit(response.data);
        this.closebutton.nativeElement.click();
      })
    }
    else {
      this.validateAllFormFields(this.form);
    }
  }

  private formValidator() {
    this.form = this.formBuilder.group({
      nameValidator: [null, [Validators.required, Validators.maxLength(50)]],
      descriptionValidator: [null, [Validators.required, Validators.maxLength(150)]],
    });
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field),
    };
  }

  isFieldValid(field: string) {
    return !this.form.get(field).valid && this.form.get(field).touched;
  }


  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(this.form.controls).forEach((field) => {
      const control = this.form.get(field);
      control.markAsTouched({ onlySelf: true });
    });
  }

  public resetFormValidators() {
    Object.keys(this.form.controls).forEach((field) => {
      const control = this.form.get(field);
      control.markAsUntouched({ onlySelf: true });
    });
  }
}
