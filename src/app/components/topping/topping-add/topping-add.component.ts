import { AfterViewInit, Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PizzaModel } from 'src/app/models/pizzaModel';
import { ToppingModel } from 'src/app/models/toppingModel';
import { ToppingService } from 'src/app/services/topping.service';

@Component({
  selector: 'app-topping-add',
  templateUrl: './topping-add.component.html',
  styleUrls: ['./topping-add.component.css']
})
export class ToppingAddComponent implements OnInit {
  @Output() responseToppingPost = new EventEmitter<ToppingModel>();
  @ViewChild('closebutton') closebutton;
  topping: ToppingModel = new ToppingModel;
  form: FormGroup;
  constructor(private formBuilder: FormBuilder,
    private ToppingService: ToppingService) { }

  ngOnInit(): void {
    this.formValidator();
  }

  onSubmit() {
    this.validateAllFormFields(this.form);
    if (this.form.valid) {
      this.ToppingService.postTopping(this.topping).subscribe(response => {
        this.responseToppingPost.emit(response.data);
        this.topping = Object.assign({}, new ToppingModel)
        this.closebutton.nativeElement.click();
      })
    }
    else {
      this.validateAllFormFields(this.form);
    }
  }

  private formValidator() {
    this.form = this.formBuilder.group({
      nameValidator: [null, [Validators.required, Validators.maxLength(50)]],
      descriptionValidator: [null, [Validators.required, Validators.maxLength(150)]],
    });
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field),
    };
  }

  isFieldValid(field: string) {
    return !this.form.get(field).valid && this.form.get(field).touched;
  }


  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(this.form.controls).forEach((field) => {
      const control = this.form.get(field);
      control.markAsTouched({ onlySelf: true });
    });
  }

  public resetFormValidators() {
    Object.keys(this.form.controls).forEach((field) => {
      const control = this.form.get(field);
      control.markAsUntouched({ onlySelf: true });
    });
  }
}
