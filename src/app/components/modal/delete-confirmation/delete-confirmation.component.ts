import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-delete-confirmation',
  templateUrl: './delete-confirmation.component.html',
  styleUrls: ['./delete-confirmation.component.css']
})
export class DeleteConfirmationComponent implements OnInit {
  @Input() modalData: any;
  @Output() confirmDelete = new EventEmitter<boolean>();
  constructor() { }

  ngOnInit(): void {
  }

  submitDelete(){
    this.confirmDelete.emit(true);
  }
}
