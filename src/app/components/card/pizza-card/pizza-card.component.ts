import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PizzaModel } from 'src/app/models/pizzaModel';

@Component({
  selector: 'app-pizza-card',
  templateUrl: './pizza-card.component.html',
  styleUrls: ['./pizza-card.component.scss']
})
export class PizzaCardComponent implements OnInit {
  @Input() pizza: PizzaModel;
  fallbackImage= "https://cdn.pixabay.com/photo/2017/12/05/20/10/pizza-3000285__340.png"
  loading: boolean = true;
  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  redirectToPizzaInformation(pizza: PizzaModel) {
    const pizzaName = pizza.id;
    this.router.navigate([`menu/${pizzaName}`])
  }

  onErrorLoadImage(): void {
    this.pizza.imageUrl = this.fallbackImage;
  }

  onImageLoad(evt): void {
    if (evt && evt.target) {
      this.loading = false;
    }
  }
}
