import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-pizza-card-skeleton',
  templateUrl: './pizza-card-skeleton.component.html',
  styleUrls: ['./pizza-card-skeleton.component.css']
})
export class PizzaCardSkeletonComponent implements OnInit {
  @Input() loading = true;
  constructor() { }

  ngOnInit(): void {
  }

}
