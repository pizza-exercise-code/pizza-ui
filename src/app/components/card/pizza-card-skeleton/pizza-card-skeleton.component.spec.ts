import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PizzaCardSkeletonComponent } from './pizza-card-skeleton.component';

describe('PizzaCardSkeletonComponent', () => {
  let component: PizzaCardSkeletonComponent;
  let fixture: ComponentFixture<PizzaCardSkeletonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PizzaCardSkeletonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PizzaCardSkeletonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
