import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PizzaModel } from 'src/app/models/pizzaModel';
import { ToppingModel } from 'src/app/models/toppingModel';
import { PizzaService } from 'src/app/services/pizza.service';

@Component({
  selector: 'app-pizza-add',
  templateUrl: './pizza-add.component.html',
  styleUrls: ['./pizza-add.component.css']
})
export class PizzaAddComponent implements OnInit {
  @Output() responsePizzaPost = new EventEmitter<PizzaModel>();
  @Input() dropdownList: ToppingModel[];
  @ViewChild('closebutton') closebutton;
  toppings: ToppingModel[] = [];
  pizza: PizzaModel = new PizzaModel;
  form: FormGroup;
  constructor(private formBuilder: FormBuilder,
    private pizzaService: PizzaService) { }

  ngOnInit(): void {
    this.formValidator();
  }

  onSubmit() {
    this.validateAllFormFields(this.form);
    if (this.form.valid) {
      this.pizzaService.postPizza(this.pizza).subscribe(response => {
        this.pizzaService.addToppingToPizza(response.data.id, this.toppings).subscribe(response => {
          this.responsePizzaPost.emit(response.data.pizza);
          this.pizza = Object.assign({}, new PizzaModel);
          this.toppings = [];
          this.closebutton.nativeElement.click();
        })
      })
    }
    else {
      this.validateAllFormFields(this.form);
    }
  }

  private formValidator() {
    this.form = this.formBuilder.group({
      nameValidator: [null, [Validators.required, Validators.maxLength(50)]],
      descriptionValidator: [null, [Validators.required, Validators.maxLength(150)]],
      priceValidator: [null, [Validators.required, Validators.min(1), Validators.max(1000)]],
      imageUrlValidator: [null, [Validators.maxLength(250)]],
      toppingsValidator: []
    });
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field),
    };
  }

  isFieldValid(field: string) {
    return !this.form.get(field).valid && this.form.get(field).touched;
  }


  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(this.form.controls).forEach((field) => {
      const control = this.form.get(field);
      control.markAsTouched({ onlySelf: true });
    });
  }

  public resetFormValidators() {
    this.toppings = [];
    Object.keys(this.form.controls).forEach((field) => {
      const control = this.form.get(field);
      control.markAsUntouched({ onlySelf: true });
    });
  }
}
