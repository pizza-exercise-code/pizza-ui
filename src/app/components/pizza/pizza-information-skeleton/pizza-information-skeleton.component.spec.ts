import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PizzaInformationSkeletonComponent } from './pizza-information-skeleton.component';

describe('PizzaInformationSkeletonComponent', () => {
  let component: PizzaInformationSkeletonComponent;
  let fixture: ComponentFixture<PizzaInformationSkeletonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PizzaInformationSkeletonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PizzaInformationSkeletonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
