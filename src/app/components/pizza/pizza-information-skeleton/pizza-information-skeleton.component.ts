import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-pizza-information-skeleton',
  templateUrl: './pizza-information-skeleton.component.html',
  styleUrls: ['./pizza-information-skeleton.component.css']
})
export class PizzaInformationSkeletonComponent implements OnInit {
  @Input() loading: boolean = true;

  constructor() { }

  ngOnInit(): void {
  }

}
