import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PizzaCollectionComponent } from './pizza-collection.component';

describe('PizzaCollectionComponent', () => {
  let component: PizzaCollectionComponent;
  let fixture: ComponentFixture<PizzaCollectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PizzaCollectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PizzaCollectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
