import { Component, Input, OnInit } from '@angular/core';
import { PizzaModel } from 'src/app/models/pizzaModel';

@Component({
  selector: 'app-pizza-collection',
  templateUrl: './pizza-collection.component.html',
  styleUrls: ['./pizza-collection.component.css']
})
export class PizzaCollectionComponent implements OnInit {
  @Input() loading: boolean = true;
  @Input() pizzas: PizzaModel[] = [];
  constructor() { }

  ngOnInit(): void {
  }
}
