import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PizzaModel } from 'src/app/models/pizzaModel';
import { PizzaService } from 'src/app/services/pizza.service';
import { ToppingService } from 'src/app/services/topping.service';
import { ToppingModel } from 'src/app/models/toppingModel';
import { Observable } from 'rxjs';
import { PizzaFlavorModel } from 'src/app/models/pizzaFlavorModel';
@Component({
  selector: 'app-pizza-update',
  templateUrl: './pizza-update.component.html',
  styleUrls: ['./pizza-update.component.css']
})
export class PizzaUpdateComponent implements OnInit {
  @Input() pizza: PizzaModel;
  @Input() toppings: ToppingModel[];
  @Output() responsePizzaPut = new EventEmitter<PizzaModel>();
  @Input() dropdownList: ToppingModel[];
  @ViewChild('closebutton') closebutton;
  cars = [];
  form: FormGroup;
  constructor(private formBuilder: FormBuilder,
    private pizzaService: PizzaService) { }

  ngOnInit(): void {
    this.formValidator();
  }

  onSubmit() {
    this.validateAllFormFields(this.form);
    if (this.form.valid) {
      this.pizzaService.updatePizza(this.pizza).subscribe(response => {
        this.pizzaService.addToppingToPizza(this.pizza.id, this.toppings).subscribe(response => {
          this.responsePizzaPut.emit(response.data.pizza);
          this.closebutton.nativeElement.click();
        }
        );
      })
    }
    else {
      this.validateAllFormFields(this.form);
    }
  }

  private formValidator() {
    this.form = this.formBuilder.group({
      nameValidator: [null, [Validators.required]],
      descriptionValidator: [null, [Validators.required]],
      priceValidator: [null, [Validators.required]],
      imageUrlValidator: [null, [Validators.maxLength(250)]],
      toppingsValidator: []
    });
  }


  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field),
    };
  }

  isFieldValid(field: string) {
    return !this.form.get(field).valid && this.form.get(field).touched;
  }


  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(this.form.controls).forEach((field) => {
      const control = this.form.get(field);
      control.markAsTouched({ onlySelf: true });
    });
  }

  public resetFormValidators() {
    Object.keys(this.form.controls).forEach((field) => {
      const control = this.form.get(field);
      control.markAsUntouched({ onlySelf: true });
    });
  }

}
