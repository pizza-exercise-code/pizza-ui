import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PizzaInformationComponent } from './pizza-information.component';

describe('PizzaInformationComponent', () => {
  let component: PizzaInformationComponent;
  let fixture: ComponentFixture<PizzaInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PizzaInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PizzaInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
