import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PizzaFlavorModel } from 'src/app/models/pizzaFlavorModel';

@Component({
  selector: 'app-pizza-information',
  templateUrl: './pizza-information.component.html',
  styleUrls: ['./pizza-information.component.scss']
})
export class PizzaInformationComponent implements OnInit {
  pizzaTitle: string = "Pizza";
  toppingsTitle: string = "Toppings";
  fallbackImage= "https://cdn.pixabay.com/photo/2017/12/05/20/10/pizza-3000285__340.png"
  @Input() pizzaFlavor: PizzaFlavorModel = new PizzaFlavorModel;
  @Input() loading: boolean = true;
  
  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  onErrorLoadImage(): void {
    this.pizzaFlavor.pizza.imageUrl = this.fallbackImage;
  }

  backToMenu () {
    this.router.navigate(['/menu']);
  }
}
