import { PizzaFlavorModel } from "src/app/models/pizzaFlavorModel";

export const pizzaFlavorMock: PizzaFlavorModel = {
  pizza: {
    name: "Hawaiana",
    description: "Pizza description",
    id: "1",
    imageUrl: 'https://cdn.pixabay.com/photo/2017/12/05/20/10/pizza-3000285__340.png',
    price: 10
  },
  toppings: [
    {
      description: "topping 1",
      id: "1",
      name: "Ham"
    },
    {
      description: "topping 2",
      id: "1",
      name: "Ham 2"
    },
    {
      description: "topping 3",
      id: "1",
      name: "Ham 3"
    }
  ]
}