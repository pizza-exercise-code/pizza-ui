export const modalData = {
  title: "Delete Topping",
  content: ""
}

export const messageModal = 'Are you sure you want to delete the topping <b> {topping} </b> from the store?'
