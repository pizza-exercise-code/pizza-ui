import { Component, OnInit } from '@angular/core';
import { ToppingModel } from 'src/app/models/toppingModel';
import { ToppingService } from 'src/app/services/topping.service';
import { messageModal, modalData } from './topping-modal-data';

@Component({
  selector: 'app-topping-table',
  templateUrl: './topping-table.component.html',
  styleUrls: ['./topping-table.component.css']
})
export class ToppingTableComponent implements OnInit {
  toppings: ToppingModel[] = [];
  modalData = modalData;
  toppingSelected: ToppingModel = new ToppingModel;
  toppingIndex: number;
  constructor(private toppingService: ToppingService) { }

  ngOnInit(): void {
    this.getToppings();
  }

  getToppings() {
    this.toppingService.getToppings().subscribe(response => {
      this.toppings = response.data;
    })
  }

  onSelectTopping(topping: ToppingModel) {
    this.toppingIndex = this.toppings.indexOf(topping);
    this.toppingSelected = Object.assign({},topping);
    this.modalData.content = messageModal.replace("{topping}", topping.name);
  }

  deleteTopping (deleteTopping) {
    if (deleteTopping) {
      this.toppingService.deleteTopping(this.toppingSelected.id).subscribe(response => {
        if (response.data == true) {
          this.deleteFromListOfToppings();
        }
      })
    }
  }

  deleteFromListOfToppings() {
    if (this.toppingIndex !== -1) {
      this.toppings.splice(this.toppingIndex, 1);
    }
  }

  addToppingToList(topping: ToppingModel) {
    this.toppings.push(topping);
  }

  public updateTopping (topping: ToppingModel) {
    this.toppings[this.toppingIndex] = Object.assign({}, topping);
  }
}
