import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ToppingTableComponent } from './topping-table.component';

describe('ToppingTableComponent', () => {
  let component: ToppingTableComponent;
  let fixture: ComponentFixture<ToppingTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ToppingTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ToppingTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
