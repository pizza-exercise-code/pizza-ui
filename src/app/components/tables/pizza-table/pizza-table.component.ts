import { Component, OnInit } from '@angular/core';
import { PizzaFlavorModel } from 'src/app/models/pizzaFlavorModel';
import { PizzaModel } from 'src/app/models/pizzaModel';
import { ToppingModel } from 'src/app/models/toppingModel';
import { PizzaService } from 'src/app/services/pizza.service';
import { ToppingService } from 'src/app/services/topping.service';
import { messageModal, modalData } from './pizza-modal-data';

@Component({
  selector: 'app-pizza-table',
  templateUrl: './pizza-table.component.html',
  styleUrls: ['./pizza-table.component.css']
})
export class PizzaTableComponent implements OnInit {
  pizzas: PizzaModel[] = [];
  pizzaSelected: PizzaModel = new PizzaModel();
  pizzaIndex: number;
  toppingSelector: ToppingModel[] = [];
  listToppings: ToppingModel[] = [];
  modalData = modalData
  constructor(private pizzaService: PizzaService,
    private toppingService: ToppingService) { }

  ngOnInit(): void {
    this.getPizzas();
  }

  getToppings() {
    this.toppingService.getToppings().subscribe(response => {
      this.listToppings = response.data;  
    })
  }
  
  getPizzas() {
    this.pizzaService.getPizzas().subscribe(response => {
      this.pizzas = response.data;
    })
  }

  onSelectPizza(pizza: PizzaModel) {
    this.getToppings();
    this.pizzaIndex = this.pizzas.indexOf(pizza);
    this.pizzaService.getPizzaAndToppings(pizza.id).subscribe(resp => {
      this.toppingSelector = resp.data.toppings;
    })
    this.pizzaSelected = Object.assign({},pizza);
    this.modalData.content = messageModal.replace("{pizzaName}", pizza.name);
  }

  addPizzaToList(pizza: PizzaModel) {
    this.pizzas.push(pizza);
  }

  deletePizza(deletePizza: true) {
    if (deletePizza) {
      this.pizzaService.deletePizza(this.pizzaSelected.id).subscribe(response => {
        if (response.data == true) {
          this.deleteFromListOfPizzas();
        }
      })
    }
  }

  deleteFromListOfPizzas() {
    if (this.pizzaIndex !== -1) {
      this.pizzas.splice(this.pizzaIndex, 1);
    }
  }

  public updatePizzaToList (pizza: PizzaModel) {
    this.pizzas[this.pizzaIndex] = Object.assign({}, pizza);
  }
}
