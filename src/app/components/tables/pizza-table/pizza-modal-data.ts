export const modalData = {
  title: "Delete Pizza",
  content: ""
}

export const messageModal = 'Are you sure you want to delete <b> {pizzaName} </b> from the store?'
