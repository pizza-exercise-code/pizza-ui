import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { heroMock } from './home-mock';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  heroMock = heroMock;
  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  navigateTo(url) {
    this.router.navigate([url]);
  }
}
