export const heroMock = {
  title: "The best <span class='text-color-secondaty'> Pizza </span> around the World",
  description: "Search, select and manage new pizzas that is on our online pizza menu",
  button: {
    label: "Order Now",
    url: "/menu"
  },
  imageUrl: 'https://cdn.pixabay.com/photo/2017/12/05/20/10/pizza-3000285__340.png'
}