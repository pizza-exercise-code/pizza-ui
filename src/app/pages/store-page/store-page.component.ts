import { Component, OnInit } from '@angular/core';
import { PizzaModel } from 'src/app/models/pizzaModel';
import { PizzaService } from 'src/app/services/pizza.service';

@Component({
  selector: 'app-store-page',
  templateUrl: './store-page.component.html',
  styleUrls: ['./store-page.component.css']
})
export class StorePageComponent implements OnInit {
  pizzas: PizzaModel[] = [];
  constructor(private pizzaService: PizzaService) { }
  loading: boolean = true;
  ngOnInit(): void {
    this.getPizzas();
  }

  getPizzas() {
    this.pizzaService.getPizzas().subscribe(response => {
      this.pizzas = response.data;
      this.loading = false;
    })
  }
}
