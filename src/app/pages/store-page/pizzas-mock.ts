import { PizzaModel } from "src/app/models/pizzaModel";

export const pizzasMock: PizzaModel[] = [{
  name: "Hawaiana",
  id: "1",
  description: "Pizza description",
  imageUrl: "https://cdn.pixabay.com/photo/2017/12/05/20/10/pizza-3000285__340.png",
  price: 15
},
{
  name: "Pepperoni",
  id: "1",
  description: "Pizza description",
  imageUrl: "https://cdn.pixabay.com/photo/2017/12/05/20/10/pizza-3000285__340.png",
  price: 10
},
{
  name: "Italian",
  id: "1",
  description: "Pizza description",
  imageUrl: "https://cdn.pixabay.com/photo/2017/12/05/20/10/pizza-3000285__340.png",
  price: 12
},
{
  name: "Muzzarella",
  id: "1",
  description: "Pizza description",
  imageUrl: "https://cdn.pixabay.com/photo/2017/12/05/20/10/pizza-3000285__340.png",
  price: 6
},
{
  name: "Meat Lovers",
  id: "1",
  description: "Pizza description",
  imageUrl: "https://cdn.pixabay.com/photo/2017/12/05/20/10/pizza-3000285__340.png",
  price: 8
},
{
  name: "Sushi Pizza",
  id: "1",
  description: "Pizza description",
  imageUrl: "https://cdn.pixabay.com/photo/2017/12/05/20/10/pizza-3000285__340.png",
  price: 15
}]