export const notFoundMessage = {
  iconUrl: "https://cdn-icons-png.flaticon.com/512/3132/3132693.png",
  status: "It looks like that the page that you are looking for does not exist. ",
  title: "Page not Found",
  button: {
    label: "Page not found",
    url: "/menu"
  }
}