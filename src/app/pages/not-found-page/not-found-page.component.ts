import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { notFoundMessage } from './not-found-message';

@Component({
  selector: 'app-not-found-page',
  templateUrl: './not-found-page.component.html',
  styleUrls: ['./not-found-page.component.scss']
})
export class NotFoundPageComponent implements OnInit {
  pageNotFound = notFoundMessage;
  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  redirectTo(redirectUrl: string) {
    this.router.navigate([redirectUrl]);
  }

}
