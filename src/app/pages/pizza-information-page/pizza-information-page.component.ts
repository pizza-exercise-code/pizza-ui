import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PizzaFlavorModel } from 'src/app/models/pizzaFlavorModel';
import { PizzaService } from 'src/app/services/pizza.service';

@Component({
  selector: 'app-pizza-information-page',
  templateUrl: './pizza-information-page.component.html',
  styleUrls: ['./pizza-information-page.component.scss']
})
export class PizzaInformationPageComponent implements OnInit {
  pizzaFlavor: PizzaFlavorModel = new PizzaFlavorModel;
  loading: boolean = true;
  pizzaTitle = "Pizza";
  constructor(private router: Router, private pizzaService: PizzaService) { }

  ngOnInit(): void {
    this.getPizzaInformation();
  }

  getPizzaInformation() {
    const pizzaId = `${window.location.href.split('menu/')[1]}`
    this.pizzaService.getPizzaAndToppings(pizzaId).subscribe(response => {
      this.pizzaFlavor = response.data;
      this.loading = false;
    });
  }
}
