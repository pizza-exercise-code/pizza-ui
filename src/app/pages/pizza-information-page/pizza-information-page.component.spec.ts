import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PizzaInformationPageComponent } from './pizza-information-page.component';

describe('PizzaInformationPageComponent', () => {
  let component: PizzaInformationPageComponent;
  let fixture: ComponentFixture<PizzaInformationPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PizzaInformationPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PizzaInformationPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
