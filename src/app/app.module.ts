import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { StorePageComponent } from './pages/store-page/store-page.component';
import { ManagerPageComponent } from './pages/manager-page/manager-page.component';
import { PizzaCardComponent } from './components/card/pizza-card/pizza-card.component';
import { PizzaInformationPageComponent } from './pages/pizza-information-page/pizza-information-page.component';
import { HttpClientModule } from '@angular/common/http';
import { PizzaTableComponent } from './components/tables/pizza-table/pizza-table.component';
import { ToppingTableComponent } from './components/tables/topping-table/topping-table.component';
import { PizzaAddComponent } from './components/pizza/pizza-add/pizza-add.component';
import { PizzaUpdateComponent } from './components/pizza/pizza-update/pizza-update.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FieldErrorDisplayComponent } from './components/field-error-display/field-error-display.component';
import { DeleteConfirmationComponent } from './components/modal/delete-confirmation/delete-confirmation.component';
import { ToppingAddComponent } from './components/topping/topping-add/topping-add.component';
import { ToppingUpdateComponent } from './components/topping/topping-update/topping-update.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { NotFoundPageComponent } from './pages/not-found-page/not-found-page.component';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { PizzaCardSkeletonComponent } from './components/card/pizza-card-skeleton/pizza-card-skeleton.component';
import { PizzaCollectionComponent } from './components/pizza/pizza-collection/pizza-collection.component';
import { PizzaInformationComponent } from './components/pizza/pizza-information/pizza-information.component';
import { PizzaInformationSkeletonComponent } from './components/pizza/pizza-information-skeleton/pizza-information-skeleton.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomePageComponent,
    StorePageComponent,
    ManagerPageComponent,
    PizzaCardComponent,
    PizzaInformationPageComponent,
    PizzaTableComponent,
    ToppingTableComponent,
    PizzaAddComponent,
    PizzaUpdateComponent,
    FieldErrorDisplayComponent,
    DeleteConfirmationComponent,
    ToppingAddComponent,
    ToppingUpdateComponent,
    NotFoundPageComponent,
    PizzaCardSkeletonComponent,
    PizzaCollectionComponent,
    PizzaInformationComponent,
    PizzaInformationSkeletonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSkeletonLoaderModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
