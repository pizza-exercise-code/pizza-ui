export class ToppingModel {
  id: string
  name: string
  description: string
  
  constructor(pizza?: Partial<ToppingModel>) {
    (<any>Object).assign(this, pizza);
  }
}