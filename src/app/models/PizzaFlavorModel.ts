import { PizzaModel } from "./pizzaModel";
import { ToppingModel } from "./toppingModel";

export class PizzaFlavorModel {
  pizza: PizzaModel
  toppings: ToppingModel[];

  constructor(pizza?: Partial<PizzaFlavorModel>) {
    (<any>Object).assign(this, pizza);
  }
}