export class ResponseModel<T> {
  status: number;
  data: T;
  error: string = null;

  constructor(pizza?: Partial<ResponseModel<T>>) {
    (<any>Object).assign(this, pizza);
  }
}