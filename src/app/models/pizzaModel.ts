export class PizzaModel {
  id: string
  name: string
  description: string
  price: number
  imageUrl: string

  constructor(pizza?: Partial<PizzaModel>) {
    (<any>Object).assign(this, pizza);
  }
}