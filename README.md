# Pizza Ui

Pizza UI is a web application to manage pizzas for an online store, This web project was generated using Angular(2+) with the 11.1.0 version.
## Install dependencies
First of all you need to install all the dependencies needed for 
the project, to do this you can use the comand `npm install`.

## Development server
Run `ng serve --open` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
